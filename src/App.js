import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import Users from './pages/Users';
import UserDetail from './pages/UserDetail';
import styled from 'styled-components';

const Container = styled.div`
  padding: 3rem 5rem;
  @media (max-width: 768px) {
    padding: 3rem 2rem;
  }
  margin-bottom: 5rem;
`;

const App = () => {
  return (
    <Container>
      <BrowserRouter>
        <Switch>
          <Route exact path="/">
            <Redirect to="/users" />
          </Route>
          <Route exact path="/users">
            <Users />
          </Route>
          <Route path="/users/:email">
            <UserDetail />
          </Route>
        </Switch>
      </BrowserRouter>
    </Container>
  );
};

export default App;
