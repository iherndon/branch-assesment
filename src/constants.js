export const ROLES = {
  DEVELOPER: 'Developer',
  APP_MANAGER: 'App Manager',
  SALES: 'Sales',
  MARKETING: 'Marketing',
  ADMIN: 'Admin',
};
