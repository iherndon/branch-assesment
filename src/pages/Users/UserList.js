import React from 'react';
import {
  TableHead,
  TableItem,
  Table,
  TableRow,
  Text,
  Link,
  TableHeading,
  MobileLabel,
} from '../../platform';
import { ROLES } from '../../constants';

export const UserList = ({ selectedEmails, data, selectUser }) => (
  <Table size={3} smallFirst>
    <TableHead>
      <TableHeading pl="1rem"></TableHeading>
      <TableHeading>
        <Text>Email</Text>
      </TableHeading>
      <TableHeading>
        <Text>Name</Text>
      </TableHeading>
      <TableHeading>
        <Text>Role</Text>
      </TableHeading>
    </TableHead>
    {data.allUsers.map((user) => (
      <TableRow
        key={user.email}
        onClick={() => selectUser(!selectedEmails[user.email], user.email)}
      >
        <TableItem pl="1rem">
          <input
            checked={!!selectedEmails[user.email]}
            type="checkbox"
            onChange={({ target: { checked } }) => selectUser(checked, user.email)}
          />
        </TableItem>
        <TableItem>
          <Link to={`users/${user.email}`}>
            <Text className="hover" variant={2} active={!!selectedEmails[user.email]}>
              {user.email}
              <MobileLabel>Email</MobileLabel>
            </Text>
          </Link>
        </TableItem>
        <TableItem>
          <Text variant={2}>{user.name}</Text>
          <MobileLabel>Name</MobileLabel>
        </TableItem>
        <TableItem>
          <Text variant={2}>{ROLES[user.role]}</Text>
          <MobileLabel>Role</MobileLabel>
        </TableItem>
      </TableRow>
    ))}
  </Table>
);
export default UserList;
