import React, { useState, useEffect } from 'react';
import { useMutation, useQuery } from '@apollo/react-hooks';
import UserHeader from '../../components/UserHeader';
import UserList from './UserList';
import { ALL_USERS_QUERY, DELETE_USERS_MUTATION } from '../../queries';

const Users = () => {
  const { loading, error, data, refetch } = useQuery(ALL_USERS_QUERY);
  const [selectedEmails, setSelectedEmails] = useState({});
  const [deleteUsers] = useMutation(DELETE_USERS_MUTATION, {
    refetchQueries: [{ query: ALL_USERS_QUERY }],
  });

  const selectUser = (checked, email) => {
    if (checked) {
      setSelectedEmails({ ...selectedEmails, [email]: email });
    } else {
      const tempSelectedEmails = { ...selectedEmails };
      delete tempSelectedEmails[email];
      setSelectedEmails(tempSelectedEmails);
    }
  };

  /* eslint-disable react-hooks/exhaustive-deps */
  useEffect(() => {
    if (data) {
      refetch();
    }
  }, []);
  /* eslint-enable */

  if (loading) {
    return <p>Loading...</p>;
  }

  if (error) {
    return <p>Error: {JSON.stringify(error)}</p>;
  }

  return (
    <>
      <UserHeader
        buttonType="outline"
        buttonText="Delete"
        title="Users"
        disabled={!Object.keys(selectedEmails).length}
        buttonColor="#ee0000"
        selectedEmails={selectedEmails}
        action={() => deleteUsers({ variables: { emails: Object.keys(selectedEmails) } })}
      />
      <UserList selectedEmails={selectedEmails} data={data} selectUser={selectUser} />
    </>
  );
};

export default Users;
