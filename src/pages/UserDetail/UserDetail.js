import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { useMutation, useQuery } from '@apollo/react-hooks';
import UserHeader from '../../components/UserHeader';
import UserData from './UserData';
import { UPDATE_USER_MUTATION, GET_USER_QUERY, ALL_USERS_QUERY } from '../../queries';

const UserDetail = () => {
  const { email } = useParams();
  const { loading, error, data } = useQuery(GET_USER_QUERY, { variables: { email } });
  const [updateUser, { loading: updateUserLoading, data: updateUserData }] = useMutation(
    UPDATE_USER_MUTATION,
    {
      refetchQueries: [{ query: ALL_USERS_QUERY }],
    }
  );
  const [userDraft, setUserDraft] = useState({ role: '', name: '' });
  const history = useHistory();

  const setUserName = ({ target: { value } }) => {
    setUserDraft({ ...userDraft, name: value });
  };

  const setUserRole = ({ target: { value } }) => {
    setUserDraft({ ...userDraft, role: value });
  };

  const saveUser = () => {
    const { email, role, name } = userDraft;
    updateUser({ variables: { email, newAttributes: { role, name } } });
  };

  useEffect(() => {
    if (data) {
      setUserDraft(data.user);
    }
  }, [data]);

  /* eslint-disable react-hooks/exhaustive-deps */
  useEffect(() => {
    if (updateUserData) {
      history.push('/users');
    }
  }, [updateUserData]);
  /* eslint-enable */

  if (loading || updateUserLoading) {
    return <p>Loading...</p>;
  }

  if (error) {
    return <p>Error: {JSON.stringify(error)}</p>;
  }
  return (
    <>
      <UserHeader title={userDraft.email} buttonColor="#0070c9" action={saveUser} mobile />
      <UserData user={userDraft} setUserName={setUserName} setUserRole={setUserRole} />
    </>
  );
};

export default UserDetail;
