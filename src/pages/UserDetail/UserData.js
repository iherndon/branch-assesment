import React from 'react';
import styled from 'styled-components';
import { Flex, Child, Text } from '../../platform';

const Container = styled(Flex)`
  border-top: 1px solid #e5e5e5;
  padding-top: 1.4rem;
  ${Child} {
    min-height: 300px;
    &:last-child {
      padding-left: 1.2rem;
      border-left: 1px solid #e5e5e5;
      div {
        margin-bottom: 0.5rem;
      }
    }
  }
`;

const Input = styled.input`
  width: 240px;
  margin-top: 0.5rem;
  padding: 0.25rem;
  border-radius: 4px;
  border: 1px solid #e5e5e5;
  max-width: 90%;
  &:focus {
    box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.05);
    border: solid 3.5px rgba(59, 153, 252, 0.5);
    outline: none;
  }
`;

const Radio = styled(Flex)`
  justify-content: flex-start;
  input {
    margin-right: 0.5rem;
  }
  margin-bottom: 0.5rem;
`;

const UserData = ({ user, setUserName, setUserRole }) => (
  <>
    <Container align="flex-start">
      <Child width="50%">
        <div>
          <Text variant={2}>Name</Text>
        </div>
        <Input type="text" value={user.name} onChange={setUserName} />
      </Child>
      <Child width="50%">
        <div>
          <Text variant={2}>Role</Text>
        </div>
        <Radio>
          <input
            type="radio"
            value="ADMIN"
            checked={user.role === 'ADMIN'}
            onChange={setUserRole}
          />
          <label>
            <Text>Admin</Text>
          </label>
        </Radio>
        <Radio>
          <input
            type="radio"
            value="DEVELOPER"
            checked={user.role === 'DEVELOPER'}
            onChange={setUserRole}
          />
          <label>
            <Text>Developer</Text>
          </label>
        </Radio>
        <Radio>
          <input
            type="radio"
            value="APP_MANAGER"
            checked={user.role === 'APP_MANAGER'}
            onChange={setUserRole}
          />
          <label>
            <Text>App Manager</Text>
          </label>
        </Radio>
        <Radio>
          <input
            type="radio"
            value="MARKETING"
            checked={user.role === 'MARKETING'}
            onChange={setUserRole}
          />
          <label>
            <Text>Marketing</Text>
          </label>
        </Radio>
        <Radio>
          <input
            type="radio"
            value="SALES"
            checked={user.role === 'SALES'}
            onChange={setUserRole}
          />
          <label>
            <Text>Sales</Text>
          </label>
        </Radio>
      </Child>
    </Container>
  </>
);

export default UserData;
