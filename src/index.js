import { ApolloProvider } from '@apollo/react-hooks';
import { createGlobalStyle } from 'styled-components';
import ApolloClient from 'apollo-boost';
import env from './env';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

const client = new ApolloClient({
  uri: env.GRAPHQL_ENDPOINT,
  request: (operation) => {
    operation.setContext({
      headers: {
        'x-api-key': env.GRAPHQL_API_KEY,
      },
    });
  },
});

const GlobalStyle = createGlobalStyle`
  html {
    font-size: 17px;
  }
  body {
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif;
  }
`;

const Root = () => (
  <ApolloProvider client={client}>
    <GlobalStyle />
    <App />
  </ApolloProvider>
);

ReactDOM.render(<Root />, document.getElementById('root'));
