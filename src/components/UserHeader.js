import React from 'react';
import { Heading, Button, Flex } from '../platform';
import styled from 'styled-components';

const FlexContainer = styled(Flex)`
  ${(props) => {
    if (props.mobile) {
      return `@media(max-width: 660px) {
      flex-direction: column;
      ${Button} {
        align-self: flex-end;
        margin-bottom: 1rem;
        width: 5rem;
        height: 2rem;
      }
    }`;
    }
  }}
`;

const UsersHeader = ({
  title,
  disabled,
  action,
  buttonColor,
  buttonType,
  buttonText = 'Save',
  mobile,
}) => (
  <FlexContainer mobile={mobile}>
    <Heading>{title}</Heading>
    <Button
      outline={buttonType === 'outline'}
      disabled={disabled}
      color={buttonColor}
      onClick={action}
    >
      {buttonText}
    </Button>
  </FlexContainer>
);

export default UsersHeader;
