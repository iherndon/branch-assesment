import styled from 'styled-components';
import { Link as RouterLink } from 'react-router-dom';

// Basic Styles
export const Heading = styled.h1`
  font-size: 2rem;
  letter-spacing: 0.37px;
`;

export const Button = styled.button`
  background-color: ${(props) => (props.outline ? 'transparent' : props.color)};
  border: ${(props) => (props.outline ? `1px solid ${props.color}` : 'none')};
  color: ${(props) => (props.outline ? props.color : 'white')};
  border-radius: 4px;
  padding: 0.35rem 1rem;
  font-size: 0.75rem;
  &:disabled {
    background-color: ${(props) => (props.outline ? 'transparent' : '#b8b8b8')};
    border: ${(props) => props.outline && `1px solid #b8b8b8`};
    color: ${(props) => (props.outline ? '#b8b8b8' : 'white')};
  }
`;

export const Flex = styled.section`
  display: flex;
  justify-content: ${(props) => props.justify || 'space-between'};
  align-items: ${(props) => props.align || 'center'};
`;

export const Child = styled.div`
  width: ${(props) => props.width};
  box-sizing: border-box;
`;

export const Text = styled.span`
  font-size: ${(props) => (props.variant === 2 ? '.75rem' : '.7rem')};
  letter-spacing: ${(props) => (props.variant === 2 ? '.14px' : '.13px')};
  color: ${(props) => (props.active ? '#0070c9' : props.variant === 2 ? '#333' : '#777')};
`;

export const Link = styled(RouterLink)`
  text-decoration: none;
`;

// Table Styles

export const MobileLabel = styled(Text)`
  display: none;
  text-transform: uppercase;
  font-size: 0.6rem;
  @media (max-width: 660px) {
    display: block;
  }
`;

export const TableHeading = styled.div`
  padding: 0.5rem 0;
  padding-left: ${(props) => props.pl};
  width: ${(props) => props.width};
  text-transform: uppercase;
`;

export const TableItem = styled.div`
  padding: 0.8rem 0;
  padding-left: ${(props) => props.pl};
  width: ${(props) => props.width};
`;

export const TableRow = styled(Flex)`
  border-bottom: solid 1px #e5e5e5;
  &:last-child {
    border-bottom: none;
  }

  &:hover {
    background-color: #fafafa;
    ${Text}.hover {
      color: #0070c9;
    }
  }
  @media (max-width: 660px) {
    display: grid;
    ${TableItem}, ${TableHeading} {
      grid-column: 1;
      &:first-child {
        grid-column: 2;
        grid-row: 2;
      }
  }
`;

export const TableHead = styled(TableRow)`
  &:hover {
    background-color: transparent;
  }
  @media (max-width: 660px) {
    display: none;
  }
`;

export const Table = styled.section`
  border-top: solid 1px #e5e5e5;
  border-bottom: solid 1px #e5e5e5;
  ${TableItem}, ${TableHeading} {
    width: ${(props) =>
      props.smallFirst ? `calc(${100 / props.size}% - 100px)` : `${100 / props.size}%`};
    &:first-child {
      width: 100px;
    }
    @media (max-width: 900px) {
      width: ${(props) =>
        props.smallFirst ? `calc(${100 / props.size}% - 25px)` : `${100 / props.size}%`};
      &:first-child {
        width: 25px;
      }
    }
  }
`;
