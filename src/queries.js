import { gql } from 'apollo-boost';

export const GET_USER_QUERY = gql`
  query GetUser($email: ID!) {
    user(email: $email) {
      email
      name
      role
    }
  }
`;

export const UPDATE_USER_MUTATION = gql`
  mutation UpdateUser($email: ID!, $newAttributes: UserAttributesInput!) {
    updateUser(email: $email, newAttributes: $newAttributes) {
      role
      name
    }
  }
`;

export const ALL_USERS_QUERY = gql`
  query {
    allUsers {
      email
      name
      role
    }
  }
`;

export const DELETE_USERS_MUTATION = gql`
  mutation DeleteUsers($emails: [ID]!) {
    deleteUsers(emails: $emails)
  }
`;
